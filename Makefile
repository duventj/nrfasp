
all:
	make -C mirf_usb/
	
clean:
	make -C mirf_usb/ clean

const:
	./h2py.py ./mirf_usb/nRF24LO1.h
	./h2py.py ./mirf_usb/usbasp_const.h 
	cat  USBASP_CONST.py  > host/usbasp_const.py
	cat  NRF24L01.py     >> host/usbasp_const.py
	