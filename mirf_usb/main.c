/*
 */
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include <avr/interrupt.h>
#include "usb.h"
#include "io.h"
#include "mirf.h"
#include "usbdrv.h"

uint8_t  main_buffer[64];
uint8_t  main_buffer_len;

void init_device(void)  {
  uint8_t i;
  // Initialize watchdog timer
  wdt_enable(WDTO_1S);
  
  // Initialize IO ports
  setup_io();
  
  // Initialize the Mirf library
  mirf_init();
  
  // Initialize the USB interface
  usbInit();
  usbDeviceDisconnect();  /* enforce re-enumeration, do this while interrupts are disabled! */
  i = 0;
  while(--i){             /* fake USB disconnect for > 250 ms */
    wdt_reset();
    _delay_ms(1);
  }
  
  usbDeviceConnect();
  sei();
  _delay_ms(10);
  mirf_config();
  
}


// Main routine
int __attribute__((noreturn)) main(void)
{ 
  uchar   i=0;
  uint8_t r=0;
  
  set_led(1);
  
  init_device();
  
  main_buffer_len = 0;

  set_led(0);
  
  for (;;) {
    wdt_reset();
    usbPoll();
    
    r = mirfPoll(&main_buffer[main_buffer_len]);

    if(r>0) 
      main_buffer_len += r;

    if(main_buffer_len>0 && usbInterruptIsReady()) {
      set_led(1);
      r = main_buffer_len - i;
      if(r > 8)
        r = 8;
      usbSetInterrupt(&main_buffer[i], r);
      i+=r;
    }
    if(i>=main_buffer_len) {
      i = main_buffer_len = 0;
      set_led(0);
    }
  }
}
