/*
    Copyright (c) 2013 Mehdi MAAREF <mehdi.maaref@hotmail.fr>

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, copy, 
    modify, merge, publish, distribute, sublicense, and/or sell copies 
    of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.

    $Id$
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <util/delay.h>

#include "usbasp_const.h"

#include "usbdrv.h"
#include "mirf.h"
#include "io.h"


// typedef enum { ok, err } success_state;

// Variables and buffers used by the USB write function
static uint8_t	buffer[64];		// Buffer to store received data in.
static uchar	currentPosition;	// Position inside the receiving buffer.
static uchar	length;	        // Number of bytes remaining.
static uchar	function;		// Function the usb write is called from.


/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

/*
* USB device descriptor.
*/
PROGMEM const char usbHidReportDescriptor[22] = {    /* USB report descriptor */
0x06, 0x00, 0xff,              // USAGE_PAGE (Generic Desktop)
0x09, 0x01,                    // USAGE (Vendor Usage 1)
0xa1, 0x01,                    // COLLECTION (Application)
0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
0x75, 0x08,                    //   REPORT_SIZE (8)
0x95, 0x01,                    //   REPORT_COUNT (1)
0x09, 0x00,                    //   USAGE (Undefined)
0xb2, 0x02, 0x01,              //   FEATURE (Data,Var,Abs,Buf)
0xc0                           // END_COLLECTION
} ;

/*
* This function is called when data is sent to endpoint 0
*/
#if USB_CFG_IMPLEMENT_FN_WRITE
uchar usbFunctionWrite(uchar *data, uchar len)
{
  uchar i=0;
  uchar done=0;
  
  if(len > length)
    len = length; 
  length -= len;
  
  for(i = 0; i < len; i++) 
    buffer[currentPosition++] = data[i];
  
  done = (length == 0);
  if(done) {
    buffer[currentPosition] = '\0';
    switch(function){
      case USB_MIRF_CFG:
        if(mirf_spi_low_cmd(&buffer[8], buffer))
           sprintf((char *)buffer, "%d", currentPosition);
        else
           sprintf((char *)buffer, "%d", 0);
        break;
      case USB_MIRF_MUL_CMD:
        // first 8 bytes is length tab
        if(mirf_spi_mul_cmd(&buffer[8], buffer))
           sprintf((char *)buffer, "%d", currentPosition);
        else
           sprintf((char *)buffer, "%d", 0);
      default:
        break;
    }
  }
  return done; // return 1 if we have all data
}
#endif

#if USB_CFG_IMPLEMENT_FN_READ
uchar usbFunctionRead(uchar *data, uchar len)
{
  return len;
}
#endif

#if USB_CFG_IMPLEMENT_FN_WRITEOUT
void usbFunctionWriteOut(uchar *data, uchar len)
{
}
#endif

/*
* Called when communicating on 'endpoint 0'
*/
USB_PUBLIC usbMsgLen_t  usbFunctionSetup(uchar data[8])
{
  usbRequest_t    *rq = (void *)data;
  unsigned val, idx;
  
  val = rq->wValue.word;  
  idx = rq->wIndex.word;   
  function = rq->bRequest;
  currentPosition = 0;                // initialize position index
  length = rq->wLength.word;  // store the amount of data requested
  if(length > sizeof(buffer)-1) // limit to buffer size
    length = sizeof(buffer)-1;
  
  /*
  * Switch between the different functions we offer on endpoint 0.
  * Beware function is reused in the usbWriteFunction routine.        
  */
  switch(function){
    // *****************************************************
    case USB_SET_LED:
      if(val)
        set_led(1);
      else
        set_led(0);
      return 0;
    case USB_GET_LED:
      if(!get_led()) 
        strcpy((char *)buffer, "on");
      else 
        strcpy((char *)buffer, "off");
    case USB_CTRL_GET_STRING:
      usbMsgPtr = buffer;         
      return strlen((char *)buffer);
    case USB_CTRL_READ:
      usbMsgPtr = &buffer[idx];         
      return length;
    case USB_MIRF_CFG:
    case USB_MIRF_MUL_CMD:
    case USB_CTRL_WRITE:
      return USB_NO_MSG;
      // *****************************************************
    default:
      return 0;   /* default for not implemented requests: return no data back to host */
  }
}

