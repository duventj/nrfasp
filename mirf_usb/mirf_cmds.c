/*
    Copyright (c) 2013 Mehdi MAAREF <mehdi.maaref@hotmail.fr>

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, copy, 
    modify, merge, publish, distribute, sublicense, and/or sell copies 
    of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.

    $Id$
*/
#include <avr/pgmspace.h>

#include "mirf.h"
#include "nRF24L01.h"

#define MIRF_CONFIG     ( (1<<MASK_MAX_RT) | (1<<MASK_RX_DR) | (1<<MASK_TX_DS) | (1<<EN_CRC) | (0<<CRCO) )


uint8_t mirf_config(void) {

  uint8_t cfg_len_buff[] = { 2, 2, 1, 6, 2, 2, 0xff, 0}; 
  uint8_t cfg_payload[] = 
  { // Set RF channel
    W_REGISTER | (REGISTER_MASK & RF_CH), 
    MIRF_CH,
    // RX_POWERUP 
    W_REGISTER | (REGISTER_MASK & CONFIG), 
    MIRF_CONFIG | ((1<<PWR_UP) | (1<<PRIM_RX)),
    NOP,
    // Set rx adress
    W_REGISTER | (REGISTER_MASK & RX_ADDR_P0), 
    0xb0, 0xb1, 0xb2, 0xb3, 0xb4,
    // Set length of incoming payload 
    W_REGISTER | (REGISTER_MASK & RX_PW_P0), 
    MIRF_PAYLOAD_SZ,
    // _spi_config_switch 
    R_REGISTER | (REGISTER_MASK & CONFIG), 
    0 
  };

  return mirf_spi_low_cmd(cfg_payload, cfg_len_buff);
}

#if 0
inline  _spi_status_config_handler(void) {
  // after first shot command are sent
  if(mirf_buffer[1] & _BV(PRIM_RX)) {
    // PRX mode
    if(mirf_buffer[0] & (_BV(RX_DR))) {
      // data available in RX_FIFO
    } else {
    }
    
  } else {
    // PTX mode
    if(mirf_buffer[0] & _BV(MAX_RT)) {
      // failed transmitting
    } else if(mirf_buffer[0] & _BV(TX_DS)) {
      // ack received, stand-by mode 2
    } else if(mirf_buffer[0] & _BV(TX_EMPTY)) {
      // TX_FIFO empty, data is sent
    } else {
      // wait for handled status with the same buffer/counter state
      mirf_buffer[0] = R_REGISTER | (REGISTER_MASK & CONFIG); // ask new status/config
      mirf_buffer[1] = 0;  
      mirf_buffer_len = 2;
    }
  }
}

uint8_t mirf_send(uint8_t channel, uint8_t *buffer, uint8_t *address) {

  if(mirf_buffer_len == 0) {
    MIRF_CE_LO;

    mirf_buffer = _mirf_buffer; // reset mirf_buffer 
    // set channel
    mirf_cmd_len[0] = 2;
    mirf_buffer[0] = W_REGISTER | (REGISTER_MASK & RF_CH);
    mirf_buffer[1] = channel;

    // set PTX
    mirf_cmd_len[1] = 2;
    mirf_buffer[2] = W_REGISTER | (REGISTER_MASK & CONFIG);
    mirf_buffer[3] = MIRF_CONFIG | ((1<<PWR_UP) | (0<<PRIM_RX));

    // set tx address
    mirf_cmd_len[2] = 6;
    mirf_buffer[4] = W_REGISTER | (REGISTER_MASK & TX_ADDR) ;
    memcpy(&mirf_buffer[5], address, 5);

    // set address for ack
    mirf_cmd_len[3] = 6;
    mirf_buffer[10] = W_REGISTER | (REGISTER_MASK & RX_ADDR_P0) ;
    memcpy(&mirf_buffer[11], address, 5);
    
    mirf_cmd_len[4] = 1;
    mirf_buffer[17] = FLUSH_TX;
    // write payload
    mirf_cmd_len[5] = 1;
    mirf_buffer[18] = W_TX_PAYLOAD;
    memcpy(&mirf_buffer[19], buffer, MIRF_PAYLOAD_SZ);
    // put CE high to trigger 
    mirf_cmd_len[6] = 0xFF;
    mirf_cmd_len[7] = 0;
    return 1;
  }

  return 0;
}

static const  uint8_t 
_spi_set_tx_cmd[] = { MIRF_PAYLOAD_SZ, 2,1,0xff,0,0,0,0 };


uint8_t  mirf_txrx(uint8_t channel, uint8_t mode) {
  uint8_t len_buff[] = { 2, 2, 2, 0xff, 0, 0, 0, 0}; 
  uint8_t payload[] = { // Set RF channel
                        W_REGISTER | (REGISTER_MASK & RF_CH), 
                        channel,
                        // RX_POWERUP 
                        W_REGISTER | (REGISTER_MASK & CONFIG), 
                        MIRF_CONFIG | ((1<<PWR_UP) | ((mode?1:0)<<PRIM_RX)),
                        // _spi_config_switch 
                        R_REGISTER | (REGISTER_MASK & CONFIG), 
                        0 };
  uint8_t r=0;

  r=mirf_spi_mul_cmd(payload, len_buff);
  if(r) {
    MIRF_CE_LO;
  }
  
  return r;
}

static const  uint8_t _spi_get_data_cmd[] = 
{ 
  R_RX_PAYLOAD,                          // Read rx payload
             0, 0,0,0,0,0,0,0,
             0, 0,0,0,0,0,0,0,
             0, 0,0,0,0,0,0,0,
             0, 0,0,0,0,0,0,0,
  W_REGISTER | (REGISTER_MASK & STATUS), // Reset status register
  1<<RX_DR,
  NOP
};
static const  uint8_t 
_spi_get_data_len[] = { MIRF_PAYLOAD_SZ, 2,1,0,0,0,0,0 };



#endif
