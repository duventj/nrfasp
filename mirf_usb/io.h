#ifndef IO_H
#define IO_H

#define isLedRedOff()   (PORTC & (1 << PC1))
#define isLedRedOn()    ~(isLedRedOff())
#define ledRedOn()    PORTC &= ~(1 << PC1)
#define ledRedOff()   PORTC |= (1 << PC1)
#define toggleLedRed()  PORTC ^= (1 << PC1)

#define isLedGreenOff()         (PORTC & (1 << PC0))
#define isLedGreenOn()          ~(isLedGreenOff())
#define ledGreenOn()            PORTC &= ~(1 << PC0)
#define ledGreenOff()           PORTC |= (1 << PC0)
#define toggleLedGreen()  PORTC ^= (1 << PC0)


#define set_led(state)  (PORTC = (!state) ?  (PORTC | (1 << PC1)) : (PORTC & ~(1 << PC1)))
#define get_led()       (PORTC & (1 << PC1))
#define toggle_led()    toggleLedRed()
#define setup_io()      do {  DDRC = 0x03;  PORTC = 0xfe; } while(0)

#endif
