#ifndef _USBASP_CONST_H_
#define _USBASP_CONST_H_

/**
 * usb custom request 
 */
#define USB_CTRL_WRITE                 0
#define USB_CTRL_READ                  1
#define USB_CTRL_GET_STRING            2

#define USB_SET_LED                    16
#define USB_GET_LED                    17

#define USB_MIRF_CFG                   32
#define USB_MIRF_MUL_CMD               33


#endif