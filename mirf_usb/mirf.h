#ifndef _MIRF_H_
#define _MIRF_H_

#include <avr/io.h>

// Mirf settings
#define MIRF_CONFIG       ( (1<<MASK_MAX_RT) | (1<<MASK_RX_DR) | (1<<MASK_TX_DS) | (1<<EN_CRC) | (0<<CRCO) )

#define MIRF_USB

// Pin definitions for chip select and chip enabled of the MiRF module
#ifdef MIRF_USB
#define CE              PD0
#define CSN             PB2
#define PORT_CE         PORTD
#define DDR_CE          DDRD
#define PORT_CSN        PORTB
#define DDR_CSN         DDRB
#else 
#define CE              PB0
#define CSN             PB1
#define PORT_CE         PORTB
#define DDR_CE          DDRB
#define PORT_CSN        PORTB
#define DDR_CSN         DDRB
#endif 

// spi port definitions
#define PORT_SPI    PORTB
#define DDR_SPI     DDRB
#define DD_MISO     DDB4
#define DD_MOSI     DDB3
#define DD_SS       DDB2
#define DD_SCK      DDB5

// Definitions for selecting and enabling MiRF module
#define MIRF_CSN_HI     PORT_CSN |=  (1<<CSN)
#define MIRF_CSN_LO     PORT_CSN &= ~(1<<CSN)
#define MIRF_CSN_ISHI   PORT_CSN &   (1<<CSN)

#define MIRF_CE_HI      PORT_CE  |=  (1<<CE)
#define MIRF_CE_LO      PORT_CE  &= ~(1<<CE)
#define MIRF_CE_IS_HI   PORT_CE  & (1<<CE)


extern uint8_t  mirfPoll(uint8_t *dataBuffer);
extern void     mirf_init(void);
extern uint8_t  mirf_config(void);

extern uint8_t mirf_spi_mul_cmd(const uint8_t *buffer, const uint8_t* len) ;
extern uint8_t mirf_spi_low_cmd(const uint8_t *buffer, const uint8_t* len) ;


#endif /* _MIRF_H_ */
