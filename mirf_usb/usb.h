
/*
 * This function is called when data is sent to endpoint 0
 */
#if USB_CFG_IMPLEMENT_FN_WRITE
extern uchar usbFunctionWrite(uchar *data, uchar len);
#endif

#if USB_CFG_IMPLEMENT_FN_READ
extern uchar usbFunctionRead(uchar *data, uchar len)
#endif

#if USB_CFG_IMPLEMENT_FN_WRITEOUT
extern void usbFunctionWriteOut(uchar *data, uchar len)
#endif
