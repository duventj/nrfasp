#!/usr/bin/env python3
# @author Mehdi MAAREF

import time
import sys, os
import threading
import getopt
from cmd import Cmd
from usbasp import *
from array import array

class CmdPromptBase(Cmd):
  def __init__(self):
    super().__init__()
    self.prompt = 'Usbasp # '
    self.stop = False
  ''' Usefull commands '''
  def emptyline(self):
    pass
  def do_shell(self, cmd):
    try:
      self.res = os.system(cmd)
    except Exception as e:
      print(e)
  def postcmd(self, stop, line):
    self.stop = stop
    return stop
  ''' Exiting '''
  def help_exit(self):
    print('Exit the command prompt')
  def do_exit(self, arg=''):
    return not print('')
  def do_EOF(self, line='') : 
    return self.do_exit('')

class UsbaspPrompt(CmdPromptBase):
  def __init__(self):
    super().__init__()
    self.device = UsbASP()
    #self.device = None
  ''' Init commands '''
  def do_mirf_start_daemon(self, args):
    pass
  def do_mirf_stop_daemon(self, args):
    pass
  ''' Basic Read Write commands '''
  def do_read_str(self, args=''):
    print(self.device.read_str())
  def do_read_buffer(self, args=''):
    print(self.device.read_buffer())
  def help_read_buffer(self):
    print('reads the usb buffer in the device')
  def do_write_buffer(self, args):
    self.device.write(args.encode('utf-8'))
  def help_write_buffer(self):
    print('send a buffer to the device')
  def do_bulk_recv(self, args=""):
    r = self.device.bulkrecv()
    print(r)
  def do_mirf_select(self, arg=None):
    if arg is None or arg is '':
      print('devices found :', len(self.device.dev))
    elif int(arg) >= len(self.device.dev):
      print('devices found :', len(self.device.dev))
    else :
      self.device.idx = int(arg)
  ''' Test commands '''
  def do_echo(self, args):
    if not args :
      args='Hello Atmega'
    self.do_write_buffer(args)
    self.do_read_buffer()
  def do_led(self, args):
    if args == "on" or args == "off":
      self.device.set_led(args)
    else:
      print(self.device.get_led())
  def help_led(self):
    print('led <on | off | status>')
  ''' MIRF commands '''
  def do_mirf_read_all(self, arg=None):
    try:
      while(1):
        self.do_bulk_recv(arg)
    except Exception:
      pass
  def do_mirf_write_register(self, args):
    args = bytes.fromhex(args)
    print(args)
    print(self.device.write_mirf_register(args[0], list(args[1:])))
    r = self.device.bulkrecv()
    print(r)
  def do_mirf_raw_cmds(self, args):
    cmd_list = list(map(lambda cmd: list(bytes.fromhex(cmd)), args.split()))
    cmd_buffer = concat_cmds(cmd_list)
    print(cmd_buffer)
    print(self.device.send_mirf_raw_multi_cmd(cmd_buffer))
    self.do_mirf_read_all()
  def do_mirf_config(self, args=''):
    print(self.device.set_rx())
    self.do_mirf_read_all()
    pass
  def do_mirf_send(self, args):
    print(self.device.send_tx(args.encode('utf-8')))
    self.do_mirf_read_all()
    pass
  def do_mirf_status(self, args):
    print(self.device.read_mirf_register())
    r = self.device.bulkrecv()
    print(r)
    pass
  def do_mirf_get_data(self, args=''):
    print(self.device.mirf_get_data())
    self.do_mirf_read_all()
  

if __name__ == "__main__":
  try:
    p = UsbaspPrompt()
    exit = False
  except Exception as e:
    exit = True
    print(e)
  while(not exit):
    try:
      p.cmdloop()
    except KeyboardInterrupt:
      print('^C')
    except Exception as e:
      if p is None:
        exit = True
      print('Error :', e)
    finally:
      exit = p.stop
    
   
    