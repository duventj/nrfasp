#!/usr/bin/env python3
# @author Mehdi MAAREF

import usb
import usb.core
from usb.util import *
import usb.control as control

from array import array
from usbasp_const import *


MIRF_CONFIG = ( (1<<MASK_MAX_RT) | (1<<MASK_RX_DR) | (1<<MASK_TX_DS) | (1<<EN_CRC) | (0<<CRCO) )
MIRF_CH = 1
MIRF_PAYLOAD_SZ = 32

GET_DATA_CMD = [ R_RX_PAYLOAD, \
                0, 0,0,0,0,0,0,0,\
                0, 0,0,0,0,0,0,0,\
                0, 0,0,0,0,0,0,0,\
                0, 0,0,0,0,0,0,0 ]
RESET_DR_CMD = [W_REGISTER | (REGISTER_MASK & STATUS), 1<<RX_DR]


def make_multi_cmd_buffer(cmd_list = [[]]*8):
  while len(cmd_list)<8:
    cmd_list.append([])
  multi_cmd_buffer = list( map(lambda x: len(x), cmd_list[0:8]))
  for i in range(0,len(cmd_list)):
    multi_cmd_buffer += cmd_list[i]
  return array('B', multi_cmd_buffer)
    
def concat_cmds(cmds):
  return list(map(lambda x: len(x), (cmds+[[]]*8)[0:8])) + sum(cmds, [])

class UsbASP(object):
  def __init__(self):
    self.dev = usb.core.find(idVendor= 0x16c0, idProduct=0x05dc, find_all=True)
    if len(self.dev) is 0:
      raise ValueError('Device not found')
    for device in self.dev:
      device.set_configuration()
      device.get_active_configuration()
    self.idx = 0
  ''' Main commands '''
  def ctrlsend(self, cmd, buffer, val=0, idx=0):
    return self.dev[self.idx].ctrl_transfer(CTRL_TYPE_VENDOR | CTRL_RECIPIENT_DEVICE | CTRL_OUT, cmd, val, idx, buffer, 500)
  def ctrlrecv(self,cmd, length=64, val=0, idx=0):
    return self.dev[self.idx].ctrl_transfer(CTRL_TYPE_VENDOR | CTRL_RECIPIENT_DEVICE | CTRL_IN, cmd, val, idx, length)
  def bulkrecv(self, length=8):
    return self.dev[self.idx].read(0x81,length)

  ''' test CMD'''
  def set_led(self, led_cmd=None):
    return self.ctrlsend(USB_SET_LED, None, led_cmd == "on")
  def get_led(self):
    return self.ctrlrecv(USB_GET_LED, 3).tobytes()

  ''' setup CMD read write '''
  def read_str(self):
    return self.ctrlrecv(USB_CTRL_GET_STRING).tobytes()

  def read_buffer(self, length = 64, val=0, idx=0):
    return self.ctrlrecv(USB_CTRL_READ, length, val, idx).tobytes()

  def write(self, buffer, val=0, idx=0):
    return self.ctrlsend(USB_CTRL_WRITE, buffer, val,  idx)

  ''' mirf spi raw '''
  def send_mirf_raw_multi_cmd(self, buffer):
    self.ctrlsend(USB_MIRF_MUL_CMD, buffer)
    return self.read_str()
  def send_mirf_raw_config_cmd(self, buffer):
    self.ctrlsend(USB_MIRF_CFG, buffer)
    return self.read_str()

  ''' mirf usb commands '''
  def read_mirf_register(self, _reg=CONFIG, length=1):
    cmd = [R_REGISTER | (REGISTER_MASK & _reg)] + [0]*length
    return self.send_mirf_raw_multi_cmd(concat_cmds([cmd]))

  def write_mirf_register(self, _reg, buffer):
    cmd = [W_REGISTER | (REGISTER_MASK & _reg)] + buffer
    return self.send_mirf_raw_multi_cmd(concat_cmds([cmd]))
  
  def send_tx(self, buffer, addr = [0xb0, 0xb1, 0xb2, 0xb3, 0xb4]):

    set_tx_mode =         [W_REGISTER | (REGISTER_MASK & CONFIG), MIRF_CONFIG | ((1<<PWR_UP) | (0<<PRIM_RX))]
    set_tx_addr =         [W_REGISTER | (REGISTER_MASK & TX_ADDR)] + addr
    set_ack_addr =        [W_REGISTER | (REGISTER_MASK & RX_ADDR_P0)] + addr
    write_payload =       [W_TX_PAYLOAD] + list(buffer + b'\0'*32)[0:32]

    cmd_list = [set_tx_mode, set_tx_addr, set_ack_addr, [FLUSH_TX], write_payload]

    cmd_buffer = concat_cmds(cmd_list)
    cmd_buffer[len(cmd_list)] = 0xfe      # set CE high
    print(len(cmd_list), cmd_buffer)
    return self.send_mirf_raw_config_cmd(cmd_buffer)

  def set_rx(self, rx_addr = [0xb0, 0xb1, 0xb2, 0xb3, 0xb4]):
    set_rf_channel =    [W_REGISTER | (REGISTER_MASK & RF_CH), MIRF_CH]
    set_rx_mode =       [W_REGISTER | (REGISTER_MASK & CONFIG), MIRF_CONFIG | ((1<<PWR_UP) | (1<<PRIM_RX))]
    set_ack_addr =      [W_REGISTER | (REGISTER_MASK & RX_ADDR_P0)] + rx_addr
    payload_length =    [W_REGISTER | (REGISTER_MASK & RX_PW_P0), MIRF_PAYLOAD_SZ]
    get_cfg =           [R_REGISTER | (REGISTER_MASK & CONFIG), 0]
    cmd_list = [set_rf_channel, set_rx_mode, set_ack_addr, payload_length, get_cfg]
    cmd_buffer = concat_cmds(cmd_list)
    cmd_buffer[len(cmd_list)] = 0xff      # set CE high
    print(len(cmd_list), cmd_buffer)
    return self.send_mirf_raw_config_cmd(cmd_buffer)

  def mirf_get_data(self):
    cmd_list = [GET_DATA_CMD, RESET_DR_CMD, [FLUSH_RX]]
    cmd_buffer = concat_cmds(cmd_list)
    return self.send_mirf_raw_multi_cmd(cmd_buffer)

if __name__ == "__main__":
  try:
    aspdev = UsbASP()
    print('ok')
  except Exception as e:
    print(e)
